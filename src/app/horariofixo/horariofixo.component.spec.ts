import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorariofixoComponent } from './horariofixo.component';

describe('HorariofixoComponent', () => {
  let component: HorariofixoComponent;
  let fixture: ComponentFixture<HorariofixoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorariofixoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorariofixoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
