import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ComunicaService} from '../comunica.service';

@Component({
  selector: 'app-dados-estagio',
  templateUrl: './dados-estagio.component.html',
  styleUrls: ['./dados-estagio.component.scss']
})

export class DadosEstagioComponent implements OnInit {

  codMat: string = " ";
  dataIni: string;
  dataFim: string;
  constructor(public dataToService: ComunicaService) { }
    
  ngOnInit() {
  
  }
  submitToService():void{
    this.dataToService.codMat = this.codMat;
    this.dataToService.dataIni = this.dataIni;
    this.dataToService.dataFim = this.dataFim;
  }
  
}
