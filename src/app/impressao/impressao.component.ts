import { Component, OnInit, Input } from '@angular/core';
import * as jsPDF from 'jspdf';
import { ComunicaService } from '../comunica.service';
import { HexaService } from '../hexa.service';
import { isUndefined } from 'util';

@Component({
  selector: 'app-impressao',
  templateUrl: './impressao.component.html',
  styleUrls: ['./impressao.component.scss']
})
export class ImpressaoComponent {
  horariosNaSemana: string = "";
  constructor(public data: ComunicaService, public dataImage: HexaService) {
  }

  gerarPDF() {
    let documento = new jsPDF();

    var imgData = this.dataImage.dataHexa;
    documento.addImage(imgData, 'JPEG', 2, 10, 205, 280);
     documento.setFontSize(12)
    //  documento.text(this.data.nomeCompleto, 55, 60);
    //  documento.text(this.data.email, 120, 74);
    //  documento.text(this.data.ra, 180, 59);
    //  documento.text(this.data.periodo, 190, 66.5);
    //  documento.setFontSize(8)
    //   documento.text(this.data.telefone, 28, 73.5);
    //   documento.text(this.data.celular, 80, 73.5);
    //   documento.setFontSize(10)
    //  documento.text(this.data.curso, 25, 66);
    //   documento.text(this.data.ano, 160, 66);
    // /*____________________________DADOS DO ESTÁGIO__________________________*/
     documento.setFontSize(15)
    // if (this.obrigatorio == 1) {
    //   documento.text('X', 57.5, 50.5);
    // } else {
    //   documento.text('X', 106.5, 50.5);
    // }
     documento.setFontSize(10)
      // documento.text(this.data.nomeEmpresa, 50, 81);
      // documento.text(this.data.nomeSupervisor, 90, 88);
      // documento.text(this.data.cargoSetor, 35, 95.5);
      // documento.text(this.data.emailSupervisor, 25, 102.5);
      // documento.text(this.data.celularSupervisor, 105, 95.5);
      // documento.text(this.data.dataIni, 37, 117);
      // documento.text(this.data.dataFim, 118, 117);
      documento.setFontSize(5)
      documento.text(this.data.horarioString, 44, 110);

    //  for(var i = 0; i < 6; i++)
    //  {
    //    console.log(i);
    //    if(this.data.horarioDoEstagio[i][0].isUndefined){
    //       i = i +1;
    //    }
    //    else if(this.data.horarioPausa[i][0].isUndefined){
    //       this.horariosNaSemana = this.horariosNaSemana + this.data.diaDaSemana[i] + 
    //       " Entrada" + this.data.horarioDoEstagio[i][0]+ this.data.horarioDoEstagio[i][1] +
    //       " Saída" + this.data.horarioDoEstagio[i][2]+ this.data.horarioDoEstagio[i][3]
    //    }else{
    //     this.horariosNaSemana = this.horariosNaSemana + this.data.diaDaSemana[i] + 
    //     " Entrada" + this.data.horarioDoEstagio[i][0]+ this.data.horarioDoEstagio[i][1] +
    //     " Saída p/ Pausa" + this.data.horarioPausa[0]+ this.data.horarioPausa[1] +
    //     " Entrada p/ Pausa" + this.data.horarioPausa[2]+ this.data.horarioPausa[3] +
    //     " Saída" + this.data.horarioDoEstagio[i][2]+ this.data.horarioDoEstagio[i][3]
    //    }
    //  }
    //  documento.text(this.horariosNaSemana,47, 110)
    // if (this.horarioFixo == 0) {
    //   documento.setFontSize(7)
    //   documento.text('SEG: ' + this.data.entradaHoraSegunda + 'h'+ this.data.entradaMinutoSegunda + ' - ' + this.data.saidaHoraSegunda + 'h'+ this.data.saidaMinutoSegunda +
    //     ' TER: ' + this.data.entradaHoraTerca + 'h'+ this.data.entradaMinutoTerca + ' - ' +                this.data.saidaHoraTerca + 'h'+ this.data.saidaMinutoTerca +
    //     ' QUA: ' + this.data.entradaHoraQuarta + 'h'+ this.data.entradaMinutoQuarta + ' - ' + this.data.saidaHoraQuarta + 'h'+ this.data.saidaMinutoQuarta +
    //     ' QUI: ' + this.data.entradaHoraQuinta + 'h'+ this.data.entradaMinutoQuinta + ' - ' + this.data.saidaHoraQuinta + 'h'+ this.data.saidaMinutoQuinta +
    //     ' SEX: ' + this.data.entradaHoraSexta + 'h'+ this.data.entradaMinutoSexta + ' - ' + this.data.saidaHoraSexta + 'h'+ this.data.saidaMinutoSexta , 47, 110);
    // }
    // else {
    //   documento.text(this.data.entradaHoraSemana + 'h'+ this.data.entradaMinutoSemana+ ' - ' 
    //   + this.data.saidaHoraSemana + 'h'+ this.data.saidaMinutoSemana, 47, 110);
    // }
    // documento.setFontSize(7)
    //  documento.text(this.data.codMat, 180, 59);

    // documento.text(this.data.horaSemanal + 'h', 186, 110);
    // documento.text(this.data.atividadesDoEstagio, 15, 134);

      // documento.text(this.data.nomeOrientador, 25, 187.5);
      // documento.text(this.data.telefoneOrientador, 35, 195);
      // documento.text(this.data.emailOrientador, 139, 195);
      // documento.text(this.data.departamento, 168, 187.5);
    documento.output("dataurlnewwindow");
    documento.save('Plano de Estagio.pdf')
  }




}
