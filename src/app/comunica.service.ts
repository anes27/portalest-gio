import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ComunicaService {
  // Dados Aluno
  ra: string;
  nomeCompleto: string;
  email: string;
  curso: string;
  telefone: string;
  celular: string;
  ano: string;
  periodo: string;
  // Dados da Empresa
  nomeEmpresa: string;
  nomeSupervisor: string;
  cargoSetor: string;
  emailSupervisor: string;
  telefoneSupervisor: string;
  celularSupervisor: string;
  // Dados do Orientador
  nomeOrientador: string;
  emailOrientador: string;
  telefoneOrientador: string;
  celularOrientador: string;
  departamento:string;
  // Dados do Estágio
  codMat: string;
  dataIni: string;
  dataFim: string;
  diaID: number = 0;
  //Horário do Estágio
  horarioString = "";
  horarioDoEstagio: any[];
  horarioPausa: any = [];
  diaDaSemana: any[6]=[
    "SEG",
    "TER",
    "QUA",
    "QUI",
    "SEX",
    "SAB",
  ]

  constructor() { }
}
