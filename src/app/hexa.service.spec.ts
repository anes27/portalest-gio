import { TestBed } from '@angular/core/testing';

import { HexaService } from './hexa.service';

describe('HexaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HexaService = TestBed.get(HexaService);
    expect(service).toBeTruthy();
  });
});
