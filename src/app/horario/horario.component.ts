import { Component, OnInit } from '@angular/core';
import { ComunicaService } from '../comunica.service';
export interface hora {
  value: string;
  hora: string;
}
export interface minuto {
  value: string;
  minuto: string;
}

@Component({
  selector: 'app-horario',
  templateUrl: './horario.component.html',
  styleUrls: ['./horario.component.scss']
})
export class HorarioComponent implements OnInit {
  horario: any = [];
  pausa: any = [];
  hora: hora[] = [
    { value: '07h', hora: '07h' },
    { value: '08h', hora: '08h' },
    { value: '09h', hora: '09h' },
    { value: '10h', hora: '10h' },
    { value: '11h', hora: '11h' },
    { value: '12h', hora: '12h' },
    { value: '13h', hora: '13h' },
    { value: '14h', hora: '14h' },
    { value: '15h', hora: '15h' },
    { value: '16h', hora: '16h' },
    { value: '17h', hora: '17h' },
    { value: '18h', hora: '18h' },
    { value: '19h', hora: '19h' },
    { value: '20h', hora: '20h' },
    { value: '21h', hora: '21h' },
    { value: '22h', hora: '22h' },
    { value: '23h', hora: '23h' },
    { value: '00h', hora: '00h' },
    { value: '01h', hora: '01h' },
    { value: '02h', hora: '02h' },
    { value: '03h', hora: '03h' },
    { value: '04h', hora: '04h' },
    { value: '05h', hora: '05h' },
    { value: '06h', hora: '06h' }
  ];
  minuto: minuto[] = [
    { value: '00', minuto: '00' },
    { value: '05', minuto: '05' },
    { value: '10', minuto: '10' },
    { value: '15', minuto: '15' },
    { value: '20', minuto: '20' },
    { value: '25', minuto: '25' },
    { value: '30', minuto: '30' },
    { value: '35', minuto: '35' },
    { value: '40', minuto: '40' },
    { value: '45', minuto: '45' },
    { value: '50', minuto: '50' },
    { value: '55', minuto: '55' }
  ];
  constructor(public dataToService: ComunicaService) { }

  ngOnInit() {
  }

  changehora(value, id): void {
    if (id == 0 || id == 1)
      this.horario[id] = value;
    else if (id == 2)
      this.pausa[0] = value;
    else if (id == 3)
      this.pausa[1] = value;
    else if (id == 4)
      this.pausa[2] = value;
    else if (id == 5)
      this.pausa[3] = value;
    else if (id == 6)
      this.horario[2] = value;
    else if (id == 7)
      this.horario[3] = value;

  }
  proximo(): void {
    this.dataToService.horarioString += this.dataToService.diaDaSemana[this.dataToService.diaID] + ": ";
    this.dataToService.horarioString +=  this.horario[0] + "" + this.horario[1]+ " ";
    if(this.pausa.length != 0){
      this.dataToService.horarioString +=  "(Pausa " + this.pausa[0] + "" + this.pausa[1]+ " ";
      this.dataToService.horarioString +=  "-" + this.pausa[2] + "" + this.pausa[3]+ ") ";
    }
    this.dataToService.horarioString +=  "- "+this.horario[2] + "" + this.horario[3]+ " | \n" ;
    
    this.dataToService.diaID+=1;
    console.log(this.dataToService.horarioString);
  }

}