import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import {ComunicaService} from '../comunica.service';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null,
    form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export interface curso {
  value: string;
  viewValue: string;
}
export interface ano {
  value: string;
  ano: number;
}
export interface periodo {
  value: string;
  periodo: number;
}

@Component({
  selector: 'app-dados-aluno',
  templateUrl: './dados-aluno.component.html',
  styleUrls: ['./dados-aluno.component.scss']
})

export class DadosAlunoComponent implements OnInit {

  ra: string;
  nome: string = "";
  email: string;
  cursoSelecionado: string;
  telefone: string;
  celular: string;
  anoSelecionado: string;
  periodoSelecionado: string;
  form: FormGroup;

  emailFormControl = new FormControl('', [Validators.required,Validators.email,]);
  raFormControl = new FormControl('', [Validators.required,]);
  nomeFormControl = new FormControl('', [Validators.required,]);
  cursoFormControl = new FormControl('', [Validators.required,]);
  telefoneFormControl = new FormControl('', [Validators.required,]);
  celularFormControl = new FormControl('', [Validators.required,]);
  anoFormControl = new FormControl('', [Validators.required,]);
  periodoFormControl = new FormControl('', [Validators.required,]);


  matcher = new MyErrorStateMatcher();
  curso: curso[] = [
    { value: 'Bacharelado em Sistemas de Informação ', viewValue: 'Bacharelado em Sistemas de Informação' }
  ];
  ano: ano[] = [
    { value: '2009', ano: 2009 },
    { value: '2010', ano: 2010 },
    { value: '2011', ano: 2011 },
    { value: '2012', ano: 2012 },
    { value: '2013', ano: 2013 },
    { value: '2014', ano: 2014 },
    { value: '2015', ano: 2015 },
    { value: '2016', ano: 2016 },
    { value: '2017', ano: 2017 },
    { value: '2018', ano: 2018 },
    { value: '2019', ano: 2019 }
  ];
  periodo: periodo[] = [
    { value: '1', periodo: 1 },
    { value: '2', periodo: 2 },
    { value: '3', periodo: 3 },
    { value: '4', periodo: 4 },
    { value: '5', periodo: 5 },
    { value: '6', periodo: 6 },
    { value: '7', periodo: 7 },
    { value: '8', periodo: 8 }

  ];
  constructor(private fb: FormBuilder, public dataToService: ComunicaService) {
  }


  ngOnInit() {
    
    this.form = this.fb.group({
      emailFormControl: new FormControl('', [Validators.required, Validators.email,]),
      raFormControl: new FormControl('', [Validators.required,]),
      nomeFormControl: new FormControl('', [Validators.required,]),
      cursoFormControl: new FormControl('', [Validators.required,]),
      telefoneFormControl: new FormControl('', [Validators.required,]),
      celularFormControl: new FormControl('', [Validators.required,]),
      anoFormControl: new FormControl('', [Validators.required,]),
      periodoFormControl: new FormControl('', [Validators.required,]),

    });
    this.form.valueChanges.subscribe(value => {
      if (this.form.invalid) {
        document.getElementById("next").setAttribute("disabled", "disabled");
      } else {
        document.getElementById("next").removeAttribute("disabled");
      }
    });
    
  }
  changeCurso(value):void{
    this.cursoSelecionado = value;
  }
  changeAno(value):void{
    this.anoSelecionado = value;
  }
  changePeriodo(value):void{
    this.periodoSelecionado = value;
  }
  submitToService():void{
    this.dataToService.ra = this.ra;
    this.dataToService.curso = this.cursoSelecionado;
    this.dataToService.email = this.email;
    this.dataToService.nomeCompleto = this.nome;
    this.dataToService.periodo = this.periodoSelecionado;
    this.dataToService.ano = this.anoSelecionado;
    this.dataToService.telefone = this.telefone;
    this.dataToService.celular = this.celular;
  }
}
