import { Component, OnInit } from '@angular/core';
import {ComunicaService} from '../comunica.service';

@Component({
  selector: 'app-dados-empresa',
  templateUrl: './dados-empresa.component.html',
  styleUrls: ['./dados-empresa.component.scss']
})
export class DadosEmpresaComponent implements OnInit {
  nomeEmpresa: string;
  nomeSupervisor: string;
  cargoSetor: string;
  email: string;
  telefone: string;
  celular: string;
  constructor(public dataToService: ComunicaService) { }

  ngOnInit() {
  }
  submitToService():void{
    this.dataToService.nomeEmpresa = this.nomeEmpresa;
    this.dataToService.nomeSupervisor = this.nomeSupervisor;
    this.dataToService.cargoSetor = this.cargoSetor;
    this.dataToService.emailSupervisor = this.email;
    this.dataToService.telefoneSupervisor = this.telefone;
    this.dataToService.celularSupervisor = this.celular;
   
  }
}
