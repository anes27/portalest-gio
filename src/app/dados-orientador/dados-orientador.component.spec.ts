import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosOrientadorComponent } from './dados-orientador.component';

describe('DadosOrientadorComponent', () => {
  let component: DadosOrientadorComponent;
  let fixture: ComponentFixture<DadosOrientadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosOrientadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosOrientadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
