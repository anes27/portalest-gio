import { Component, OnInit } from '@angular/core';
import {ComunicaService} from '../comunica.service';

@Component({
  selector: 'app-dados-orientador',
  templateUrl: './dados-orientador.component.html',
  styleUrls: ['./dados-orientador.component.scss']
})
export class DadosOrientadorComponent implements OnInit {
  nome: string;
  email: string;
  telefone: string;
  celular: string;
  departamento:string;
  constructor(public dataToService: ComunicaService) { }

  ngOnInit() {
  }

  submitToService():void{
    this.dataToService.nomeOrientador = this.nome;
    this.dataToService.emailOrientador = this.email;
    this.dataToService.telefoneOrientador = this.telefone;
    this.dataToService.celularOrientador = this.celular;
    this.dataToService.departamento = this.departamento;
  }
}
